#!/bin/bash
# author mfrank
# install all the best brew packages
set -x

# brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# brew packages
brew install $(<list.txt)

# brew casks
brew cask install $(<casklist.txt)

cp -f ./mac-upgrade.sh /usr/local/bin/mac-upgrade

set +x
echo "All casks are now installed!"
