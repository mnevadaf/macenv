#!/bin/bash
# author mfrank
# make your mac "brand new again", this will update all sw, delete old sw, delete all caches, trash, etc.

set -x # print commands
set +e # don't exit on error

# Brew
brew update
brew upgrade
brew cask upgrade
brew cleanup -s -v
brew doctor
brew missing

# Rust
rustup update

# pip fresh reinstall all packages to home dir (no system-wide site packages)
pip freeze | sed s/=.*// >req2.txt
pip3 freeze | sed s/=.*// >req3.txt
pip uninstall -y -r req2.txt
pip3 uninstall -y -r req3.txt
pip --no-cache-dir install --upgrade --force-reinstall --user -r req2.txt
pip3 --no-cache-dir install --upgrade --force-reinstall --user -r req3.txt
rm -f req2.txt req3.txt

# Docker
docker ps -q | xargs -L1 docker stop
docker system prune -a -f
test -z "$(docker ps -q 2>/dev/null)" && osascript -e 'quit app "Docker"' # if all the containers exited successfully, then restart docker.
open --background -a Docker

# Vagrant
vagrant box prune -f
vagrant box list | cut -f 1 -d ' ' | xargs -L 1 vagrant box remove -f

# MacOS system clean & update
rm -rvf "$HOME"/.Trash/* "$HOME"/Library/Saved Application State/* "$HOME"/Library/Caches/*
softwareupdate -i -a

set +x
echo "Your mac is now clean. To install any OS updates, you need to reboot."
