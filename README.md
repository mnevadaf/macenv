# Provisioning a new mac

1. Clone / Download this repo
2. Terminal > `cd Directory/of/this/repo` > `bash mac-install.sh`

# Upgrading an existing mac to latest
This will remove all old software, containers, caches, etc and upgrade to the latest _everything_.

1. Terminal > `mac-upgrade`

Provisioning or upgrading can be run multiple times without harm.

Once completed, you still may need to reboot to install OS updates.

# Updating brew packages
These are all the top-level brew packages & casks. To update them according to your local config, and commit them
```
cd /Directory/of/this/repo
git checkout -b myupdatedcasks
brew leaves > list.txt
brew cask list > casklist.txt
git add list.txt casklist.txt
git commit -m "Updating brew packages & casks"
```

These lists are only used on initial provisioning. Upgrade will always upgrade _everything_ that is currently installed, regardless of whether it's in the list.txt or casklist.txt.
